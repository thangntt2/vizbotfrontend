
import React from 'react'
import { includes } from 'lodash'
import style from './style.css'

const entityTypes = [
  'trait',
  'keyword',
  'freetext',
]

const EntityTypeInTable = (props) => {
  const { types } = props
  return (
    <div>
      { entityTypes.map(entityType => (
        <span className={includes(entityType, types) ? style.entityTypeInTableOn : style.entityTypeInTableOff}>
          {entityType}
        </span>
      ))}
    </div>
  )
}

EntityTypeInTable.propTypes = {
  types: React.PropTypes.array.isRequired,
}

export default EntityTypeInTable
