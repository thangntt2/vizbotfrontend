
import React from 'react'
import { Row, Col } from 'react-bootstrap'
import { Entity } from 'draft-js'
import style from './style.css'

const getEntityFromCharList = (charlist) => {
  const entityKeySet = new Set([])
  const entityList = []

  charlist.forEach((char) => {
    const entity = char.getEntity()
    if (entity && (!(entityKeySet.has(entity)))) {
      entityKeySet.add(entity)
      entityList.push(Entity.get(entity).getData())
    }
  })
  return entityList
}

const InlineEntityList = (props) => {
  const { charlist } = props
  const listEntities = getEntityFromCharList(charlist)

  return (
    <ul>
      {listEntities && listEntities.length > 0 &&
        listEntities.map(entity => (
          <Row className={style.entity} key={`${entity.name}_${entity.value}`}>
            <Col xs={12} md={4} style={{ background: entity.color }}>{entity.name}</Col>
            <Col xs={12} md={8}>{entity.value}</Col>
          </Row>
        ))
      }
    </ul>
  )
}

InlineEntityList.propTypes = {
  charlist: React.PropTypes.object,
}

export default InlineEntityList
