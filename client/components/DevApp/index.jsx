
import React from 'react'
import { Glyphicon, Button, Panel } from 'react-bootstrap'
import { Editor } from 'draft-js'
import 'react-select/dist/react-select.css'
import { Creatable } from 'react-select'
// import Creatable from './Creatable'
import InlineEntityList from './InlineEntityList'
import EntityList from './EntityList'
import style from './style.css'

const Devapp = (props) => {
  const {
    onNewEntityNameChange,
    addNewEntity,
    editorState,
    onChange2,
    entityList,
    onSubmit,
  } = props

  const selectState = editorState.getSelection()
  const selection = editorState
    .getCurrentContent()
    .getPlainText()
    .substring(selectState.getStartOffset(), selectState.getEndOffset())

  return (
    <div className={style.container}>
      <Panel bsStyle="primary">
        Add more sentence to train your bott
        <div
          style={{
            background: '#eee',
            borderBottom: '1px solid #ccc',
            padding: '5px',
          }}
        >
          <Editor
            editorState={editorState}
            onChange={onChange2}
            className={style.inputSentence}
          />
        </div>
        <InlineEntityList charlist={editorState.getCurrentContent().getFirstBlock().getCharacterList()} />
        <div className={style.addEntityCrew}>
          <Glyphicon className={style.addEntitySign} glyph="plus-sign" />
          <Creatable
            className={style.addEntityInput}
            placeholder={selection && selection.length > 0 ? `Create new entity for "${selection}"` : 'Add new entity'}
            options={entityList && entityList.map(entity => ({
              value: entity,
              label: entity.name,
            }))}
            promptTextCreator={label => (<span>Press Enter to Create new entity named <b>{label}</b>.</span>)}
            onChange={onNewEntityNameChange}
            onNewOptionClick={addNewEntity}
            inputProps={{
              style: {
                border: 'none',
              },
            }}
          />
        </div>
        <Button bsStyle="success" onClick={onSubmit} >
          <Glyphicon className={style.addEntity} glyph="ok" />
          Add
        </Button>
      </Panel>
      <hr className={style.hr} />
      { entityList && entityList.length > 0 &&
        <EntityList
          entityList={entityList}
        />
      }
    </div>
  )
}

Devapp.propTypes = {
  onNewEntityNameChange: React.PropTypes.func,
  addNewEntity: React.PropTypes.func,
  editorState: React.PropTypes.object,
  onChange2: React.PropTypes.func,
  entityList: React.PropTypes.array,
  onSubmit: React.PropTypes.func,
}

export default Devapp
