
import React from 'react'
import { Table } from 'react-bootstrap'
import style from './style.css'

const EntityList = (props) => {
  const { entityList } = props
  return (
    <div>
      <h3>Your app has 8 entities</h3>
      <Table striped>
        <thead>
          <tr>
            <th>Name</th>
            <th>Search strategy</th>
            <th>Values</th>
          </tr>
        </thead>
        <tbody>
          {entityList.map(entity => (
            <tr key={entity.name}>
              <td>
                <div style={{ marginBottom: '10px' }}>
                  <span
                    className={style.entityName}
                    style={{
                      backgroundColor: entity.color,
                    }}
                    onClick={() => { console.log(entity.name) }}
                  >
                    {entity.name}
                  </span>
                </div>
              </td>
              <td>{entity.type}</td>
              <td>Test</td>
            </tr>
          ))}
        </tbody>
      </Table>
    </div>
  )
}

EntityList.propTypes = {
  entityList: React.PropTypes.array,
}

export default EntityList
