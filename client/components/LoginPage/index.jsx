
import React from 'react'

const LoginPage = (props) => {
  const { loginFunc } = props
  return (
    <div>
    Login Page
      <div><button onClick={() => { loginFunc('github') }} >github</button></div>
      <div><button onClick={() => { loginFunc('google-oauth2') }} >google</button></div>
    </div>
  )
}

LoginPage.propTypes = {
  loginFunc: React.PropTypes.func.isRequired,
}

export default LoginPage

