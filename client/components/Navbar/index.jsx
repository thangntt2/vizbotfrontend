
import React from 'react'
import style from './style.css'
import Logos from '../../assets/logo.png'

class StickyNavbar extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      top: 0,
      left: 40,
    }
  }

  render() {
    const { listRoute, isLoggedIn, login, logout } = this.props
    return (
      <div className={style.navContainer} >
        <header>
          <nav className={style.nav} >
            <img alt="" className={style.subLogo} src={Logos} />
            <ul className={style.listRoute}>
              {listRoute && listRoute.map(route => (
                <li key={route}><a className={style.headerlink}>{route}</a></li>
              ))}
              {isLoggedIn
                ? <li><a className={style.headerlink} onClick={login}>Login</a></li>
                : <li><a className={style.headerlink} onClick={logout}>Logout</a></li>
              }
            </ul>
          </nav>
        </header>
      </div>
    )
  }
}

StickyNavbar.propTypes = {
  listRoute: React.PropTypes.arrayOf(React.PropTypes.string).isRequired,
  isLoggedIn: React.PropTypes.bool.isRequired,
  login: React.PropTypes.func.isRequired,
  logout: React.PropTypes.func.isRequired,
}

export default StickyNavbar
