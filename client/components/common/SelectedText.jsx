
import React from 'react'

const SelectedText = props => (
  <span
    style={{
      backgroundColor: 'rgba(6,155,250,.2)',
      color: '#069bfa',
    }}
  >
    {props.children}
  </span>
)

SelectedText.propTypes = {
  children: React.PropTypes.array,
}

export default SelectedText
