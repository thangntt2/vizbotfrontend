
import React from 'react'
import { Entity } from 'draft-js'

const InlineEntity = (props) => {
  const entity = Entity.get(props.entityKey)
  return (
    <span
      style={{
        backgroundColor: entity.getData().color,
      }}
    >
      {props.children}
    </span>
  )
}

InlineEntity.propTypes = {
  entityKey: React.PropTypes.string,
  children: React.PropTypes.array,
}

export default InlineEntity
