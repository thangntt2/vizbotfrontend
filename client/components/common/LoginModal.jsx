import React from 'react'
import { Modal } from 'react-bootstrap'

const LoginModal = (props) => {
  const { isShow, closeAction } = props
  return (
    <Modal
      show={isShow}
      onHide={closeAction}
    >
      <Modal.Header closeButton>
        <Modal.Title>Login</Modal.Title>
      </Modal.Header>
    </Modal>
  )
}

LoginModal.propTypes = {
  isShow: React.PropTypes.bool,
  closeAction: React.PropTypes.func.isRequired,
}

export default LoginModal
