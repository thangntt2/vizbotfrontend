
import { createAction } from 'redux-actions'
import { omit } from 'lodash'

export const createData = createAction('SUBMIT', data => omit(data, 'meta'), data => data.meta)
export const submitting = createAction('SUBMITTING', state => state)
export const fetching = createAction('FETCHING', state => state)
export const dataReceived = createAction('DATA_RECEIVED', data => data)
export const fetchData = createAction('FETCH_DATA', dataType => dataType)
export const createNewEntityInlineEntity = createAction('CREATE_ENTITY_INLINEENTITY', entity => entity)
export const createNewEntityFromNewEntity = createAction('CREATE_NEW_INLINEENTITY_FROM_NEW', entity => entity)

export const loginSuccess = createAction('LOGIN_SUCCESS', data => data)
export const logoutRequest = createAction('LOGOUT_REQUEST')
export const logoutLocal = createAction('LOGOUT_LOCAL')
export const loginRequest = createAction('LOGIN_REQUEST')
export const loginSocial = createAction('LOGIN_SOCIAL', data => data)

export const addNotification = createAction('ADD_NOTIFICATION', data => data)
export const removeNotification = createAction('REMOVE_NOTIFICATION', data => data)
