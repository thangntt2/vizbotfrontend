
import { routerReducer as routing } from 'react-router-redux'
import { combineReducers } from 'redux'
import auth from './auth'

const submit = (state = { submitting: false }, action) => {
  if (action.type === 'SUBMITTING') {
    return {
      ...state,
      submitting: action.payload.submitting,
    }
  }
  return state
}

const data = (state = {}, action) => {
  if (action.type === 'DATA_RECEIVED') {
    return {
      ...state,
      ...action.payload,
    }
  }
  return state
}

const newInlineEntityFromEntity = (state = {}, action) => {
  if (action.type === 'CREATE_NEW_INLINEENTITY_FROM_NEW') {
    return {
      ...state,
      ...action.payload,
    }
  }
  return state
}

const pushNotification = (state = {}, action) => {
  if (action.type === 'ADD_NOTIFICATION') {
    const notification = action.payload
    return {
      ...state,
      ...notification,
    }
  } else if (action.type === 'REMOVE_NOTIFICATION') {
    return {
      ...state,
      notification: undefined,
    }
  }
  return state
}

export default combineReducers({
  routing,
  submit,
  data,
  newInlineEntityFromEntity,
  auth,
  pushNotification,
})
