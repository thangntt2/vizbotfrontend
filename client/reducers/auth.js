const auth = (state = { accessToken: undefined }, action) => {
  switch (action.type) {
    case 'LOGIN_REQUEST': {
      return ({
        ...state,
        submitting: true,
      })
    }
    case 'LOGIN_SUCCESS': {
      const { response } = action.payload
      return ({
        ...state,
        ...response,
        expiresIn: response.expiresIn + (new Date()).getTime(),
        submitting: false,
      })
    }
    case 'LOGIN_FAILURE': {
      return ({
        ...state,
        submitting: false,
      })
    }
    case 'LOGOUT_LOCAL': {
      return ({
        ...state,
        accessToken: undefined,
        idToken: undefined,
      })
    }
    default: {
      if (action.error && action.error.status && action.error.status === 401) {
        return ({
          ...state,
          accessToken: undefined,
        })
      }
      return state
    }
  }
}

export default auth
