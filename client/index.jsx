
import { Router, Route, browserHistory } from 'react-router'
import { syncHistoryWithStore } from 'react-router-redux'
import { Provider } from 'react-redux'
import ReactDOM from 'react-dom'
import React from 'react'
import { getStoredState, createPersistor } from 'redux-persist'

import rootSaga from './saga'
import App from './containers/App'
import DevAppContainer from './containers/DevAppContainer'
import configureStore from './store'

const persistConfig = { whitelist: ['auth'] }

getStoredState(persistConfig, (err, restoredState) => {
  const store = configureStore(restoredState)
  store.runSaga(rootSaga)
  const history = syncHistoryWithStore(browserHistory, store)
  createPersistor(store, persistConfig)

  ReactDOM.render(
    <Provider store={store}>
      <Router history={history}>
        <Route path="/" component={App}>
          <Route path="/devbot" component={DevAppContainer} />
        </Route>
      </Router>
    </Provider>,
    document.getElementById('root'),
  )
})
