
import { put, call, fork, select } from 'redux-saga/effects'
import { takeEvery } from 'redux-saga'
import * as actions from '../actions'
import * as apis from '../apis'

const jwtDecode = require('jwt-decode')

const getIdToken = state => state.auth.idToken

function* pushNewNotification(notification) {
  const time = (new Date()).getMilliseconds()
  yield put(actions.addNotification({ ...notification, time }))
  // yield call(delay, 2000)
  // yield put(actions.removeNotification({ ...notification, time }))
}

function* pushSuccessNoti(message) {
  yield call(pushNewNotification, { message, type: 'SUCCESS' })
}

function* pushErrorNoti(message) {
  yield call(pushNewNotification, { message, type: 'ERROR' })
}

//  handlers
function* submitEntity(entity) {
  yield put(actions.submitting(true))
  const idToken = yield select(getIdToken)
  const { response, error } = yield call(apis.Entity.submit, entity, idToken)
  yield put(actions.submitting(false))
  if (!error) {
    yield put(actions.dataReceived({
      entity: JSON.parse(response.text),
    }))
    return JSON.parse(response.text)
  }
  return false
}

function* fetchEntityList() {
  yield put(actions.fetching(true))
  const idToken = yield select(getIdToken)
  const { response, error } = yield call(apis.Entity.fetchEntityList, idToken)
  yield put(actions.fetching(false))
  if (!error) {
    yield put(actions.dataReceived({
      entityList: JSON.parse(response.text),
    }))
  }
}

function* submitExpression(expression) {
  yield put(actions.submitting(true))
  const idToken = yield select(getIdToken)
  const { response, error } = yield call(apis.Expression.submit, expression, idToken)
  yield put(actions.submitting(false))
  if (!error) {
    yield put(actions.dataReceived({
      expression: JSON.parse(response.text),
    }))
    return JSON.parse(response.text)
  }
  return false
}

function* loginSocial(payload) {
  const { site } = payload
  const { response, error } = yield call(apis.auth.loginSocial, site)
  if (!error) {
    return response
  }
  return undefined
}

//  do stubs
function* doSubmitData(action) {
  const { payload, meta } = action
  switch (meta.dataType) {
    case 'entity': {
      const response = yield call(submitEntity, payload)
      if (response) {
        yield call(pushSuccessNoti, `Created new Entity: ${response.name}`)
      } else {
        yield call(pushErrorNoti, 'Cannot created new Entity')
      }
      break
    }
    case 'expression': {
      const response = yield call(submitExpression, payload)
      if (response) {
        yield call(pushSuccessNoti, `Created new Expression: ${response.sentence}`)
      } else {
        yield call(pushErrorNoti, 'Cannot created new Expression')
      }
      break
    }
    default:
      console.log('not implemented yet data getter')
  }
}

function* doFetchData(action) {
  const { payload } = action
  switch (payload) {
    case 'entityList':
      yield call(fetchEntityList)
      break
    default:
      console.log('not implemented yet data getter')
  }
}

function* doCreateEntityInlineEntity(action) {
  const { payload } = action
  const entity = yield call(submitEntity, payload)
  if (entity) {
    yield put(actions.createNewEntityFromNewEntity(entity))
  }
}

function* doLoginSocial(action) {
  const { payload } = action
  const response = yield call(loginSocial, payload)
  const data = yield call(jwtDecode, response.idToken)
  if (data) {
    yield put(actions.loginSuccess({ response }))
    yield call(pushSuccessNoti, `Welcome ${data.name}`)
  } else {
    yield call(pushErrorNoti, 'Please check your login')
  }
}

function* doLogout() {
  yield call(apis.auth.logout)
  yield put(actions.logoutLocal())
}

//  sagas watcher
function* watchSubmitData() {
  yield takeEvery('SUBMIT', doSubmitData)
}

function* watchFetchData() {
  yield takeEvery('FETCH_DATA', doFetchData)
}

function* watchCreateEntityInlineEntity() {
  yield takeEvery('CREATE_ENTITY_INLINEENTITY', doCreateEntityInlineEntity)
}

function* watchLogin() {
  yield takeEvery('LOGIN_SOCIAL', doLoginSocial)
}

function* watchLogout() {
  yield takeEvery('LOGOUT_REQUEST', doLogout)
}

export default function* root() {
  yield [
    fork(watchSubmitData),
    fork(watchFetchData),
    fork(watchCreateEntityInlineEntity),
    fork(watchLogin),
    fork(watchLogout),
  ]
}
