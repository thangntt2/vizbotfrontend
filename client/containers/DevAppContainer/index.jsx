
import React from 'react'
import { connect } from 'react-redux'
import { EditorState, Entity, Modifier, CompositeDecorator } from 'draft-js'
import Devapp from '../../components/DevApp'
import InlineEntity from '../../components/common/InlineEntity'
import SelectedText from '../../components/common/SelectedText'
import { createData, fetchData, createNewEntityInlineEntity } from '../../actions'

function findLinkEntities(contentBlock, callback) {
  contentBlock.findEntityRanges(
    (character) => {
      const entityKey = character.getEntity()
      return (
        entityKey !== null &&
        Entity.get(entityKey).getType() === 'LINK'
      )
    },
    callback,
  )
}

function findSelectEntity(contentBlock, callback) {
  contentBlock.findEntityRanges(
    (character) => {
      const entityKey = character.getEntity()
      return (
        entityKey !== null &&
        Entity.get(entityKey).getType() === 'SELECTION'
      )
    },
    callback,
  )
}

function applyEntity(editorState, selectState, entityKey) {
  let newEditorState = editorState
  const newContent = Modifier.applyEntity(
    newEditorState.getCurrentContent(),
    selectState,
    entityKey,
  )

  newEditorState = EditorState.push(
    newEditorState,
    newContent,
    'apply-entity',
  )
  return newEditorState
}

const getEntityFromCharList = (charlist) => {
  const entityKeySet = new Set([])
  const entityList = []

  charlist.forEach((char) => {
    const entity = char.getEntity()
    if (entity && (!(entityKeySet.has(entity)))) {
      entityKeySet.add(entity)
      const entityData = Entity.get(entity).getData()
      entityList.push({
        entityId: entityData.entityId,
        start: entityData.start,
        end: entityData.end,
      })
    }
  })
  return entityList
}

class DevAppContainer extends React.Component {
  constructor(props) {
    super(props)
    this.handleNewEntityNameChange = this.handleNewEntityNameChange.bind(this)
    this.addNewEntity = this.addNewEntity.bind(this)
    this.addInlineEntity = this.addInlineEntity.bind(this)
    this.handleChange = this.handleChange.bind(this)
    this.handleSubmitExpression = this.handleSubmitExpression.bind(this)

    const decorator = new CompositeDecorator([
      {
        strategy: findLinkEntities,
        component: InlineEntity,
      }, {
        strategy: findSelectEntity,
        component: SelectedText,
      },
    ])

    this.state = {
      newEntityName: {},
      editorState: EditorState.createEmpty(decorator),
    }
  }

  componentWillMount() {
    this.props.fetchData('entityList')
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.entity !== this.props.entity) { //  it means we received new entity after create
      this.props.fetchData('entityList')
    }
    if (nextProps.newInlineEntityFromEntity !== this.props.newInlineEntityFromEntity) {
      this.addInlineEntity(nextProps.newInlineEntityFromEntity)
    }
    if (nextProps.expression !== this.props.expression) {
      const decorator = new CompositeDecorator([
        {
          strategy: findLinkEntities,
          component: InlineEntity,
        },
      ])
      this.setState({
        editorState: EditorState.createEmpty(decorator),
      })
    }
  }

  addInlineEntity(entity) {
    const selectState = this.state.editorState.getSelection()
    const contentState = this.state.editorState.getCurrentContent()

    const entityKey = Entity.create(
      'LINK',
      'IMMUTABLE',
      {
        ...entity,
        entityId: entity.id,
        start: selectState.getStartOffset(),
        end: selectState.getEndOffset(),
        value: contentState.getPlainText().substring(selectState.getStartOffset(), selectState.getEndOffset()),
      },
    )

    const newContent = Modifier.applyEntity(
      contentState,
      selectState,
      entityKey,
    )

    let editorState = EditorState.push(
      this.state.editorState,
      newContent,
      'apply-entity',
    )

    editorState = EditorState.moveFocusToEnd(editorState)

    this.setState({
      editorState,
    })
  }

  handleNewEntityNameChange(option) {
    if (option.value.color) {
      this.addInlineEntity(option.value)
    } else {
      const selectState = this.state.editorState.getSelection()
      this.props.createNewEntityInlineEntity({
        name: option.label,
        type: selectState.getStartOffset() !== selectState.getEndOffset() ? 'keyword' : 'trait',
      })
    }
  }

  handleChange(editorState) {
    this.setState({ editorState })
    const selectState = editorState.getSelection()
    let newEditorState = editorState

    if (selectState.getStartOffset() !== selectState.getEndOffset()) {
      if (this.state.selectEntityKey) {
        const entityData = Entity.get(this.state.selectEntityKey).getData()
        if (entityData.start === selectState.getStartOffset() && entityData.end === selectState.getEndOffset()) {
          this.setState({ editorState })
          return
        }
        newEditorState = applyEntity(
          editorState,
          this.state.editorState.getSelection(),
          null,
        )
      }
      const entityKey = Entity.create(
        'SELECTION',
        'MUTABLE',
        {
          start: selectState.getStartOffset(),
          end: selectState.getEndOffset(),
        },
      )
      newEditorState = applyEntity(newEditorState, selectState, entityKey)
      this.setState({
        editorState: newEditorState,
        selectEntityKey: entityKey,
      })
      return
    }
    if (this.state.selectEntityKey) {
      const oldEntityData = Entity.get(this.state.selectEntityKey).getData()
      if (oldEntityData.start === selectState.getStartOffset() && oldEntityData.end === selectState.getEndOffset()) {
        this.setState({ editorState })
        return
      }
      if (oldEntityData.start !== oldEntityData.end) {
        newEditorState = applyEntity(
          newEditorState,
          this.state.editorState.getSelection(),
          null,
        )
        newEditorState = EditorState.forceSelection(
          newEditorState,
          selectState,
        )
        this.setState({ editorState: newEditorState })
      }
      if (this.state.selectEntityKey) {
        Entity.replaceData(
          this.state.selectEntityKey,
          {
            start: selectState.getStartOffset(),
            end: selectState.getEndOffset(),
          },
        )
      }
    }
  }

  addNewEntity(selected) {
    const selectState = this.state.editorState.getSelection()

    this.props.createData({
      name: selected.label,
      type: selectState.getStartOffset() !== selectState.getEndOffset() ? 'keyword' : 'trait',
      meta: {
        dataType: 'entity',
      },
    })
  }

  handleSubmitExpression() {
    this.props.createData({
      sentence: this.state.editorState.getCurrentContent().getPlainText(),
      inlineEntities: getEntityFromCharList(
        this.state.editorState.getCurrentContent().getFirstBlock().getCharacterList(),
      ),
      meta: {
        dataType: 'expression',
      },
    })
  }

  render() {
    return (
      <Devapp
        newEntityName={this.state.newEntityName}
        onNewEntityNameChange={this.handleNewEntityNameChange}
        addNewEntity={this.addNewEntity}
        editorState={this.state.editorState}
        onChange2={this.handleChange}
        entityList={this.props.entityList}
        onSubmit={this.handleSubmitExpression}
      />
    )
  }
}

function mapStateToProp(state) {
  const {
    data: {
      entityList,
      entity,
      expression,
    },
    newInlineEntityFromEntity,
  } = state
  return {
    entityList,
    entity,
    newInlineEntityFromEntity,
    expression,
  }
}

DevAppContainer.propTypes = {
  createData: React.PropTypes.func,
  fetchData: React.PropTypes.func,
  createNewEntityInlineEntity: React.PropTypes.func,
  entityList: React.PropTypes.array,
  entity: React.PropTypes.object,
  expression: React.PropTypes.object,
  newInlineEntityFromEntity: React.PropTypes.object,
}

export default connect(mapStateToProp, {
  createData,
  fetchData,
  createNewEntityInlineEntity,
})(DevAppContainer)
