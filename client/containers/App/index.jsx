import React from 'react'
import { connect } from 'react-redux'
import { NotificationStack } from 'react-notification'
import StickyNavbar from '../../components/Navbar'
import LoginPage from '../../components/LoginPage'
import style from './style.css'
import { loginSocial, logoutRequest, loginRequest } from '../../actions'

const listRoute = [
  'showcase',
  'research',
  'blog',
  'photography',
]

class App extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      stickyStatus: true,
    }
    this.onStickyStateChange = this.onStickyStateChange.bind(this)
    this.checkAuth = this.checkAuth.bind(this)
    this.handleLogin = this.handleLogin.bind(this)
    this.state = {
      notiStack: [],
    }
    this.removeNotification = this.removeNotification.bind(this)
  }

  componentWillMount() {
    this.checkAuth()
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.pushNotification && nextProps.pushNotification !== this.props.pushNotification) {
      const newNoti = nextProps.pushNotification
      this.setState({
        notiStack: this.state.notiStack.concat({
          message: newNoti.message,
          key: `${newNoti.message}|${newNoti.time}`,
          action: 'Dismiss',
          onClick: () => {
            this.removeNotification(`${newNoti.message}|${newNoti.time}`)
          },
        }),
      })
    }
  }

  onStickyStateChange(isSticky) {
    if (isSticky) {
      this.setState({ stickyStatus: true })
    } else {
      this.setState({ stickyStatus: true })
    }
  }

  removeNotification(key) {
    this.setState({
      notiStack: this.state.notiStack.filter(n => n.key !== key),
    })
  }

  checkAuth() {
    const { auth, routing } = this.props

    if (routing.locationBeforeTransitions.pathname !== '/login' &&
      routing.locationBeforeTransitions.pathname !== '/logout') {
      if (!auth.idToken || !auth.expiresIn || auth.expiresIn * 1000 < (new Date()).getTime()) {
        return false
      }
    }
    return true
  }

  handleLogin(site) {
    this.props.loginSocial({ site })
  }

  render() {
    return (
      <div>
        {this.checkAuth()
          ? <div>
            <StickyNavbar
              stickyStatus={this.state.stickyStatus}
              listRoute={listRoute}
              isLoggedIn={!this.checkAuth()}
              login={this.props.loginRequest}
              logout={this.props.logoutRequest}
            />
            <div className={style.container}>
              {!this.state.showLogin &&
                React.Children.toArray(this.props.children)}
            </div>
            <NotificationStack
              notifications={this.state.notiStack}
              onDismiss={notification => this.removeNotification(notification.key)}
              dismissAfter={5000}
            />
          </div>
          : <LoginPage loginFunc={this.handleLogin} />
        }
      </div>
    )
  }
}

App.propTypes = {
  children: React.PropTypes.node,
  auth: React.PropTypes.object.isRequired,
  routing: React.PropTypes.object.isRequired,
  loginSocial: React.PropTypes.func.isRequired,
  logoutRequest: React.PropTypes.func.isRequired,
  loginRequest: React.PropTypes.func.isRequired,
  pushNotification: React.PropTypes.object.isRequired,
}

function mapStateToProps(state) {
  const { auth, routing, pushNotification } = state
  return { auth, routing, pushNotification }
}

export default connect(mapStateToProps, {
  loginSocial,
  logoutRequest,
  loginRequest,
})(App)
