
import request from 'superagent'

const API_BASE = process.env.API_BASE || 'http://52.77.212.240:8080/api/v1'

export const submit = (entity, idToken) => (
  request.post(`${API_BASE}/entity`)
    .send(entity)
    .set('Authorization', idToken)
    .then(response => ({ response }))
    .catch(error => ({ error }))
)

export const fetchEntityList = idToken => (
  request.get(`${API_BASE}/entity`)
    .set('Authorization', idToken)
    .then(response => ({ response }))
    .catch(error => ({ error }))
)
