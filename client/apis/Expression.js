
import request from 'superagent'

const API_BASE = process.env.API_BASE || 'http://52.77.212.240:8080/api/v1'

export const submit = (expression, idToken) => (
  request.post(`${API_BASE}/expression`)
    .send(expression)
    .set('Authorization', idToken)
    .then(response => ({ response }))
    .catch(error => ({ error }))
)
