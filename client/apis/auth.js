import auth0 from 'auth0-js'
import request from 'superagent'
// import popupAuth from '../helper/loginpopup'

const auth = new auth0.WebAuth({
  domain: 'thangntt.au.auth0.com',
  clientID: 'Gs2T8fA2dABZXgRzK9YSotAedAZ4fLgy',
  responseType: 'token',
})

const authen = new auth0.Authentication({
  domain: 'thangntt.au.auth0.com',
  clientID: 'Gs2T8fA2dABZXgRzK9YSotAedAZ4fLgy',
  responseType: 'token',
})

export function loginSocial(site) {
  // const authURI = (authen.buildAuthorizeUrl({
  //   connection: site,
  //   state: 'login',
  //   redirectUri: 'http://localhost:3000/logincallback',
  //   scope: 'openid name email nickname',
  // }))
  // return new Promise((resolve, reject) => {
  //   popupAuth(authURI, 'http://localhost:3000/logincallback', (data, err) => {
  //     if (err) {
  //       reject(err)
  //     }
  //     resolve(data)
  //   })
  // })
  // .then(response => ({ response }))
  // .catch(error => ({ error }))
  return new Promise((resolve, reject) => {
    auth.popup.authorize({
      connection: site,
      redirectUri: 'http://localhost:3000/logincallback',
      scope: 'openid name email nickname',
    }, (err, response) => {
      resolve(response)
      if (err) {
        reject(err)
      }
    })
  })
  .then(response => ({ response }))
  .catch(error => ({ error }))
}

export function logout() {
  const logoutUri = (authen.buildLogoutUrl({
    client_id: 'Gs2T8fA2dABZXgRzK9YSotAedAZ4fLgy',
  }))
  return request.get(logoutUri)
    .then(response => ({ response }))
    .catch(error => ({ error }))
}
